use crate::prelude::*;

impl UIButton {
    pub unsafe fn update(&mut self, _assets: &mut AssetManager, state: EButtonState) {
        self.change_state(state)
    }

    pub unsafe fn draw(&mut self, assets: &mut AssetManager) {
        let font = *assets.get_font("sonoma");

        match self.state {
            EButtonState::Normal => unsafe {
                //draw button
                let position = rl::Vector2 {
                    x: self.rec.x,
                    y: self.rec.y,
                };

                let texture = *assets.get_texture("ui_button01");

                rl::DrawTextureV(texture, position, GameColor::WHITE.into());

                //draw text of button
                rl::DrawTextEx(
                    font,
                    rl_str!(self.text),
                    rl::Vector2 {
                        x: self.rec.x + 40.0,
                        y: self.rec.y + 20.0,
                    },
                    self.fontsize,
                    0.0,
                    self.textcolor.into(),
                )
            },

            EButtonState::Hover => unsafe {
                //draw button
                let position = rl::Vector2 {
                    x: self.rec.x,
                    y: self.rec.y,
                };

                let texture = *assets.get_texture("ui_button01hover");

                rl::DrawTextureV(texture, position, GameColor::WHITE.into());

                //draw text of button
                rl::DrawTextEx(
                    font,
                    rl_str!(self.text),
                    rl::Vector2 {
                        x: self.rec.x + 40.0,
                        y: self.rec.y + 20.0,
                    },
                    self.fontsize,
                    0.0,
                    GameColor::RAYWHITE.into(),
                )
            },

            EButtonState::Selected => unsafe {
                //draw button
                let position = rl::Vector2 {
                    x: self.rec.x,
                    y: self.rec.y,
                };

                let texture = *assets.get_texture("ui_button01select");

                rl::DrawTextureV(texture, position, GameColor::WHITE.into());

                //draw text of button
                rl::DrawTextEx(
                    font,
                    rl_str!(self.text),
                    rl::Vector2 {
                        x: self.rec.x + 40.0,
                        y: self.rec.y + 20.0,
                    },
                    self.fontsize,
                    0.0,
                    GameColor::NIGHTBLACK.into(),
                )
            },
        }
    }
}
