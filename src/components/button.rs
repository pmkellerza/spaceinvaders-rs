use crate::prelude::*;
#[derive(Clone)]
pub enum EButtonState {
    Normal,
    Hover,
    Selected,
}

#[derive(Clone)]
pub struct UIButton {
    pub text: String,
    pub rec: rl::Rectangle,
    pub state: EButtonState,
    pub fontsize: f32,

    pub textcolor: GameColor,
    pub function: unsafe fn(assets: &mut AssetManager),
}

impl UIButton {
    pub fn new(
        width: f32,
        height: f32,
        text: impl Into<String>,
        textcolor: GameColor,
        fontsize: f32,
    ) -> Self {
        let text = text.into();

        let rec = rl::Rectangle {
            x: 0.0,
            y: 0.0,
            width,
            height,
        };

        Self {
            text,
            rec,
            textcolor,
            fontsize,
            state: EButtonState::Normal,
            function: empty,
        }
    }

    pub fn with_position(mut self, pos_x: f32, pos_y: f32) -> Self {
        self.rec.x = pos_x;
        self.rec.y = pos_y;

        self
    }
    pub fn set_position(&mut self, pos_x: f32, pos_y: f32) {
        self.rec.x = pos_x;
        self.rec.y = pos_y;
    }

    pub fn change_state(&mut self, state: EButtonState) {
        self.state = state;
    }
}

unsafe fn empty(_assets: &mut AssetManager) {}
