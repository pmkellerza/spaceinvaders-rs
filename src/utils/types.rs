#[allow(unused_imports)]
use crate::prelude::*;
use hashbrown::HashMap;
use std::cell::RefCell;
use std::rc::Rc;

pub type RefAssetManager = Rc<RefCell<AssetManager>>;

//AssetManager Types
pub type Textures = HashMap<&'static str, rl::Texture2D>;
pub type Sounds = HashMap<&'static str, rl::Sound>;
pub type Fonts = HashMap<&'static str, rl::Font>;
pub type JukeBox = HashMap<&'static str, MusicTrack>;
pub type Shaders = HashMap<&'static str, rl::Shader>;

//UI
pub type Buttons = HashMap<&'static str, UIButton>;
