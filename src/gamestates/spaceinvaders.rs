use crate::gamestates::utils::gamestates::get_game_state;
use crate::prelude::*;

pub mod entities;
mod scripts;

pub struct SpaceInvaders {
    //safezone
    safe_zone: rl::Rectangle,

    //stars
    starfield: StarField,
    meteorshower: MeteoriteShower,
}

impl SpaceInvaders {
    pub unsafe fn new(_assets: &mut AssetManager) -> Self {
        let safe_zone = rl::Rectangle {
            x: SAFE_PADDINGF32,
            y: SAFE_PADDINGF32,
            width: SAFE_WIDTHF32,
            height: SAFE_HEIGHTF32,
        };

        Self {
            safe_zone,
            starfield: StarField::new(100),
            meteorshower: MeteoriteShower::new(18),
        }
    }

    pub unsafe fn update(&mut self, assets: &mut AssetManager) {
        //Continue playing Music Stream
        update_jukebox(assets);

        self.starfield.update();

        self.meteorshower.update();

        input(assets);
    }
    pub unsafe fn draw(&mut self, assets: &mut AssetManager) {
        rl::ClearBackground(GameColor::NIGHTBLACK.into());

        rl::DrawRectangleRec(self.safe_zone, GameColor::NIGHTBLACK.into());

        self.starfield.draw();

        self.meteorshower.draw();

        //---- Drawn Last

        //ui border
        let texture = assets.get_texture("ui_border");
        rl::DrawTexture(*texture, 0, 0, GameColor::WHITE.into());
    }
}

unsafe fn input(assets: &mut AssetManager) {
    // Music Control
    if rl::IsKeyPressed(rl::KeyboardKey_KEY_LEFT_BRACKET as i32) {
        *MUSIC_IDX.lock().unwrap() -= 1;

        next_music_idx(assets);
    }

    if rl::IsKeyPressed(rl::KeyboardKey_KEY_RIGHT_BRACKET as i32) {
        *MUSIC_IDX.lock().unwrap() += 1;

        next_music_idx(assets);
    }

    //PauseMenu
    if rl::IsKeyPressed(rl::KeyboardKey_KEY_ESCAPE as i32) {
        assets
            .jukebox
            .pause_music(*CURRENT_MUSIC_TRACK.lock().unwrap());
        *GAME_STATE.lock().unwrap() = get_game_state(3);
    }
}

impl Drop for SpaceInvaders {
    fn drop(&mut self) {
        println!("SpaceInvaders GameMode Dropped");
    }
}
