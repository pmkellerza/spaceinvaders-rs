use crate::prelude::*;
use rand::Rng;

pub struct MysteryShip {
    pub transform: Transform2D,
    pub velocity: glm::Vec2,

    pub color: GameColor,
    pub alive: bool,

    pub scale_limit: f32,
    pub scale_min: f32,
    pub scale_factor: f32,
}

impl MysteryShip {
    pub unsafe fn new(position: glm::Vec2, velocity: glm::Vec2, color: GameColor) -> Self {
        let transform = Transform2D::new(position);
        let scale_limit = transform.scale.x;
        let scale_min = scale_limit - 0.1;
        Self {
            transform,
            velocity,
            color,
            alive: true,
            scale_limit,
            scale_min,
            scale_factor: 1.1,
        }
    }

    pub unsafe fn random_color(&mut self) {
        let mut rng = thread_rng();

        match rng.gen_range(1..=7) {
            1 => self.color = GameColor::ICEBLUE,
            2 => self.color = GameColor::RAYWHITE,
            3 => self.color = GameColor::CHILLRED,
            4 => self.color = GameColor::RAYWHITE,
            5 => self.color = GameColor::DARKMAGENTA,
            6 => self.color = GameColor::KELLYGREEN,
            7 => self.color = GameColor::RAYWHITE,
            _ => unreachable!(),
        }
    }
}
