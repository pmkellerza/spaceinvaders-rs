use crate::prelude::*;

pub struct Star {
    position: glm::Vec2,
    size: f32,
    color: rl::Color,
}

impl Star {
    pub unsafe fn new() -> Star {
        let mut rnd = thread_rng();

        let position = glm::Vec2::new(
            rnd.gen_range(SAFE_PADDINGF32..SAFE_WIDTHF32),
            rnd.gen_range(SAFE_PADDINGF32..SAFE_HEIGHTF32),
        );

        let size = rnd.gen_range(1.0..=2.0);
        let color = rl::ColorAlpha(GameColor::MINTCREAM.into(), rnd.gen_range(100.0..=255.0));

        Self {
            position,
            size,
            color,
        }
    }

    unsafe fn change_color(&mut self, num: u8) {
        match num {
            1 => self.color = GameColor::MINTCREAM.into(),
            2 => self.color = GameColor::DARKMAGENTA.into(),
            3 => self.color = GameColor::CHILLRED.into(),
            4 => self.color = GameColor::MINTCREAM.into(),
            5 => self.color = GameColor::ICEBLUE.into(),
            _ => unreachable!(),
        }
    }
}

pub struct StarField {
    stars: Vec<Star>,
}

impl StarField {
    pub unsafe fn new(num: usize) -> Self {
        let mut stars = Vec::with_capacity(num);

        for _ in 0..num {
            stars.push(Star::new())
        }

        Self { stars }
    }

    pub unsafe fn update(&mut self) {
        for star in self.stars.iter_mut() {
            star.position.x += thread_rng().gen_range(-1.0..=1.0) * rl::GetFrameTime();

            star.change_color(thread_rng().gen_range(1..=5));
            star.color.a = thread_rng().gen_range(150..=255);
        }
    }

    pub unsafe fn draw(&mut self) {
        for star in &self.stars {
            rl::DrawCircle(
                star.position.x as i32,
                star.position.y as i32,
                star.size,
                star.color,
            )
        }
    }
}
