use crate::prelude::*;

#[derive(Debug)]
pub enum EAlienType {
    Alien1(&'static str, GameColor),
    Alien2(&'static str, GameColor),
    Alien3(&'static str, GameColor),
}

impl EAlienType {
    pub const ALIEN1: EAlienType = EAlienType::Alien1("alien_1", GameColor::ICEBLUE);
    pub const ALIEN2: EAlienType = EAlienType::Alien2("alien_2", GameColor::KELLYGREEN);
    pub const ALIEN3: EAlienType = EAlienType::Alien3("alien_3", GameColor::CORNELLRED);
}

impl EAlienType {
    pub fn get_data(&self) -> (&'static str, GameColor) {
        return match self {
            EAlienType::Alien1(id, color) => (id, *color),
            EAlienType::Alien2(id, color) => (id, *color),
            EAlienType::Alien3(id, color) => (id, *color),
        };
    }
}

#[derive(Debug)]
pub struct Alien {
    pub transform: Transform2D,
    pub velocity: glm::Vec2,

    pub texture_id: &'static str,
    pub color: GameColor,
    pub alive: bool,
}

impl Alien {
    pub unsafe fn new(position: glm::Vec2, alien_type: EAlienType) -> Self {
        let transform = Transform2D::new(position);
        let velocity = glm::Vec2::zeros();

        let (texture_id, color) = alien_type.get_data();

        Self {
            transform,
            velocity,
            texture_id,
            color,
            alive: true,
        }
    }

    pub unsafe fn with_velocity(mut self, velocity: glm::Vec2) -> Self {
        self.velocity = velocity;
        self
    }
}
