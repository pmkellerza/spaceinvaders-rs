pub mod entities;

mod scripts;

use crate::prelude::*;
use entities::{gametitle::*, mainmenu::*};

pub struct TitleScreen {
    //safezone
    safe_zone: rl::Rectangle,

    //stars
    starfield: StarField,
    meteorshower: MeteoriteShower,

    //Titles
    title: GameTitle,

    //UI
    menu: MainMenu,

    //Entities
    mysteryship: MysteryShip,
    aliens: Vec<Alien>,
}

impl TitleScreen {
    pub unsafe fn new(assets: &mut AssetManager) -> Self {
        let safe_zone = rl::Rectangle {
            x: SAFE_PADDINGF32,
            y: SAFE_PADDINGF32,
            width: SAFE_WIDTHF32,
            height: SAFE_HEIGHTF32,
        };

        let mysteryship = MysteryShip::new(
            glm::Vec2::new(-200.0, SAFE_PADDINGF32 + 200.0),
            glm::Vec2::new(1.0, 0.0),
            GameColor::ICEBLUE,
        );

        let aliens = vec![
            Alien::new(glm::Vec2::new(600.0, 435.0), EAlienType::ALIEN1)
                .with_velocity(glm::Vec2::new(0.35, 0.0)),
            Alien::new(glm::Vec2::new(330.0, 405.0), EAlienType::ALIEN2)
                .with_velocity(glm::Vec2::new(-0.5, 0.0)),
            Alien::new(glm::Vec2::new(130.0, 515.0), EAlienType::ALIEN3)
                .with_velocity(glm::Vec2::new(0.65, 0.0)),
        ];

        //start music playing
        assets
            .jukebox
            .play_music(*CURRENT_MUSIC_TRACK.lock().unwrap());

        Self {
            starfield: StarField::new(100),
            title: GameTitle::new(),
            meteorshower: MeteoriteShower::new(24),
            safe_zone,
            mysteryship,
            aliens,
            menu: MainMenu::new(),
        }
    }

    pub unsafe fn update(&mut self, assets: &mut AssetManager) {
        //Check music playing status
        /* if assets
            .jukebox
            .is_playing_music(*CURRENT_MUSIC_TRACK.lock().unwrap())
        {
            update_jukebox(assets);
        } else {
            assets
                .jukebox
                .resume_music(*CURRENT_MUSIC_TRACK.lock().unwrap());
            update_jukebox(assets);
        }*/

        update_jukebox(assets);

        self.starfield.update();

        self.meteorshower.update();

        self.title.update();

        self.mysteryship.update(assets);

        self.menu.update(assets);

        for alien in &mut self.aliens {
            alien.update(assets);
        }
    }

    pub unsafe fn draw(&mut self, assets: &mut AssetManager) {
        rl::ClearBackground(GameColor::NIGHTBLACK.into());

        rl::DrawRectangleRec(self.safe_zone, GameColor::NIGHTBLACK.into());

        self.starfield.draw();

        self.meteorshower.draw();

        self.title.draw(assets);

        self.mysteryship.draw(assets);

        for alien in &mut self.aliens {
            alien.draw(assets);
        }

        //Music Player Track
        draw_music_text(
            "--- Currently Playing ---",
            assets,
            20.0,
            55.0,
            GameColor::ICEBLUE,
        );

        //let font = assets.get_font("sonoma");
        let musictrack = assets.get_musictrack(*CURRENT_MUSIC_TRACK.lock().unwrap());
        let music_msg = format!(
            "Track: {}  -  Artist: {}",
            musictrack.name, musictrack.artist
        );
        draw_music_text(music_msg, assets, 16.0, 25.0, GameColor::RAYWHITE);

        let msg = format!(
            "Track#: {} - Time: {:.2}/100.00",
            *MUSIC_IDX.lock().unwrap(),
            assets
                .jukebox
                .get_timeplayed(*CURRENT_MUSIC_TRACK.lock().unwrap())
        );
        draw_music_text(msg, assets, 16.0, 5.0, GameColor::CHILLRED);

        //---------- Always Last drawn ---------
        //Main Menu

        self.menu.draw(assets);

        //ui border
        let texture = assets.get_texture("ui_border");
        rl::DrawTexture(*texture, 0, 0, GameColor::WHITE.into());
    }
}

unsafe fn draw_music_text(
    msg: impl Into<String>,
    assets: &mut AssetManager,
    fontsize: f32,
    pixelshift: f32,
    color: GameColor,
) {
    let msg = msg.into();
    let font = assets.get_font("sonoma");
    rl::DrawTextEx(
        *font,
        rl_str!(msg),
        rl::Vector2 {
            x: SAFE_WIDTHF32 / 2.0 - ((fontsize / 2.0) * (msg.len() as f32 / 3.0)),
            y: SAFE_HEIGHTF32 - pixelshift,
        },
        fontsize,
        0.0,
        color.into(),
    );
}
