use crate::gamestates::utils::gamestates::get_game_state;
use crate::prelude::*;

pub struct Menu {
    //buttons
    pub buttons: Buttons,

    //Menu
    pub menu_idx: u8,
}

impl Menu {
    pub unsafe fn new(_assets: &mut AssetManager) -> Self {
        //button
        let padding = 20.0;
        let width = 230.0;
        let height = 60.0;
        let pos_x = SCREEN_WIDTHF32 / 2.0 - width / 2.0;

        let mut resume_btn =
            UIButton::new(width, height, "Resume Game", GameColor::NIGHTBLACK, 20.0)
                .with_position(pos_x, 630.0);
        resume_btn.function = btn_resume;

        let mut exit_btn = UIButton::new(width, height, "End Game", GameColor::NIGHTBLACK, 20.0)
            .with_position(pos_x, 630.0 + height + padding);
        exit_btn.function = btn_end;

        let mut buttons = Buttons::new();
        buttons.insert("resume", resume_btn);
        buttons.insert("end", exit_btn);

        Self {
            buttons,
            menu_idx: 1,
        }
    }
}

unsafe fn btn_resume(assets: &mut AssetManager) {
    assets
        .jukebox
        .resume_music(*CURRENT_MUSIC_TRACK.lock().unwrap());

    *GAME_STATE.lock().unwrap() = get_game_state(2);
}

unsafe fn btn_end(assets: &mut AssetManager) {
    assets
        .jukebox
        .resume_music(*CURRENT_MUSIC_TRACK.lock().unwrap());

    *GAME_ENDED.lock().unwrap() = true;
    *GAME_STATE.lock().unwrap() = get_game_state(1);
}
