use crate::gamestates::pausemenu::entities::menu::Menu;
use crate::prelude::*;

impl GameSyncWithAsset for Menu {
    unsafe fn update(&mut self, assets: &mut AssetManager) {
        //Menu Pinput Controls
        if rl::IsKeyPressed(rl::KeyboardKey_KEY_UP as i32)
            || rl::IsKeyPressed(rl::KeyboardKey_KEY_W as i32)
        {
            //playsound
            let sound = assets.get_sound("menu_select");
            rl::PlaySound(*sound);

            self.menu_idx -= 1
        }
        if rl::IsKeyPressed(rl::KeyboardKey_KEY_DOWN as i32)
            || rl::IsKeyPressed(rl::KeyboardKey_KEY_S as i32)
        {
            //play sound
            let sound = assets.get_sound("menu_select");
            rl::PlaySound(*sound);

            self.menu_idx += 1;
        }

        if rl::IsKeyPressed(rl::KeyboardKey_KEY_ENTER as i32) {
            //play sound
            match self.menu_idx {
                1 => {
                    let func = self.buttons.get_mut("resume").unwrap().function;
                    func(assets);
                }
                2 => {
                    let func = self.buttons.get_mut("end").unwrap().function;
                    func(assets);
                }
                _ => unreachable!(),
            }
        }

        //Menu Index Control
        if self.menu_idx > 2 {
            self.menu_idx = 1
        };
        if self.menu_idx == 0 {
            self.menu_idx = 2
        };

        match self.menu_idx {
            1 => {
                self.buttons.get_mut("resume").unwrap().state = EButtonState::Hover;
                self.buttons.get_mut("end").unwrap().state = EButtonState::Normal;
            }
            2 => {
                self.buttons.get_mut("resume").unwrap().state = EButtonState::Normal;
                self.buttons.get_mut("end").unwrap().state = EButtonState::Hover;
            }
            _ => unreachable!(),
        }
    }

    unsafe fn draw(&mut self, assets: &mut AssetManager) {
        for button in self.buttons.values_mut() {
            button.draw(assets)
        }
    }
}
