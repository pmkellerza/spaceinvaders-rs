use crate::gamestates::pausemenu::entities::menu::Menu;
use crate::gamestates::titlescreen::entities::gametitle::GameTitle;
use crate::gamestates::utils::gamestates::get_game_state;
use crate::prelude::*;
mod entities;
mod scripts;

pub struct PauseMenu {
    //menu
    menu: Menu,
}

impl PauseMenu {
    pub unsafe fn new(assets: &mut AssetManager) -> Self {
        Self {
            menu: Menu::new(assets),
        }
    }

    pub unsafe fn update(&mut self, assets: &mut AssetManager) {
        update_jukebox(assets);

        input(assets);

        self.menu.update(assets);
    }
    pub unsafe fn draw(&mut self, assets: &mut AssetManager) {
        let mnu_x = SAFE_PADDING + 135;
        let mnu_y = SAFE_PADDING + 175;

        let texture = assets.get_texture("pmenu_backgnd");
        let width = texture.width as f32;
        let _height = texture.height as f32;

        rl::DrawTexture(*texture, mnu_x, mnu_y, GameColor::WHITE.into());

        let font = assets.get_font("sonoma");
        rl::DrawTextEx(
            *font,
            rl_str!("PAUSE MENU"),
            rl::Vector2 {
                x: (mnu_x as f32 + width) / 2.0,
                y: mnu_y as f32 + 10.0,
            },
            24.0,
            0.0,
            GameColor::NIGHTBLACK.into(),
        );

        //Last to Draw
        //Gametitle
        let space_font = assets.get_font("acrylic");
        rl::DrawTextEx(
            *space_font,
            rl_str!("SPACE"),
            rl::Vector2 { x: 250.0, y: 50.0 },
            60.0,
            0.0,
            GameColor::ORANGEPEEL.into(),
        );

        let invader_font = assets.get_font("acrylic");
        rl::DrawTextEx(
            *invader_font,
            rl_str!("Invaders"),
            rl::Vector2 { x: 285.0, y: 95.0 },
            40.0,
            0.0,
            GameColor::CHILLRED.into(),
        );

        self.menu.draw(assets);
    }
}

unsafe fn input(assets: &mut AssetManager) {
    if rl::IsKeyPressed(rl::KeyboardKey_KEY_ESCAPE as i32) {
        assets
            .jukebox
            .resume_music(*CURRENT_MUSIC_TRACK.lock().unwrap());

        *GAME_STATE.lock().unwrap() = get_game_state(2);
    }
}
