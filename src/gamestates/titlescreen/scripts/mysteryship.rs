use crate::prelude::*;

impl GameSyncWithAsset for MysteryShip {
    unsafe fn update(&mut self, _assets: &mut AssetManager) {
        let mut rng = thread_rng();

        //random glitch scaling
        self.transform.scale.x = rng.gen_range(self.scale_min..=self.scale_limit);

        if self.transform.scale.x >= self.scale_limit || self.transform.scale.x <= self.scale_min {
            self.scale_factor *= -1.0;
        }

        self.transform.scale.x = rng.gen_range(self.scale_min..=self.scale_limit);

        self.transform.position += self.velocity;

        if self.transform.position.x >= SCREEN_WIDTHF32 {
            self.transform.position.x = -200.0;
            self.transform.position.y = rng.gen_range((SAFE_PADDINGF32 + 200.0)..=350.0);

            self.transform.scale.x = rng.gen_range(1.0..=3.0);
            self.scale_limit = self.transform.scale.x;
            self.scale_min = self.scale_limit - 0.1;

            self.random_color();
        }
    }

    unsafe fn draw(&mut self, assets: &mut AssetManager) {
        let texture = assets.get_texture("mysteryship");

        if self.transform.position.x >= 0.0 - texture.width as f32
            && self.transform.position.x <= SAFE_WIDTHF32 + texture.width as f32
        {
            rl::DrawTextureEx(
                *texture,
                self.transform.position.into_vec2(),
                self.transform.rotation.x,
                self.transform.scale.x,
                self.color.into(),
            );
        }
    }
}
