use crate::gamestates::titlescreen::entities::mainmenu::MainMenu;
use crate::prelude::*;

impl GameSyncWithAsset for MainMenu {
    unsafe fn update(&mut self, assets: &mut AssetManager) {
        self.timer += rl::GetFrameTime();

        if self.timer >= self.max_timer {
            self.ready = true;
        }

        if self.ready {
            //input controls
            if rl::IsKeyPressed(rl::KeyboardKey_KEY_UP as i32)
                || rl::IsKeyPressed(rl::KeyboardKey_KEY_W as i32)
            {
                //playsound
                let sound = assets.get_sound("menu_select");
                rl::PlaySound(*sound);

                self.menu_idx -= 1
            }
            if rl::IsKeyPressed(rl::KeyboardKey_KEY_DOWN as i32)
                || rl::IsKeyPressed(rl::KeyboardKey_KEY_S as i32)
            {
                //play sound
                let sound = assets.get_sound("menu_select");
                rl::PlaySound(*sound);

                self.menu_idx += 1;
            }

            if rl::IsKeyPressed(rl::KeyboardKey_KEY_ENTER as i32) {
                //play sound
                match self.menu_idx {
                    1 => {
                        let func = self.buttons.get_mut("play").unwrap().function;
                        func(assets);
                    }
                    2 => {
                        let func = self.buttons.get_mut("exit").unwrap().function;
                        func(assets);
                    }
                    _ => unreachable!(),
                }
            }

            //Menu Index Control
            if self.menu_idx > 2 {
                self.menu_idx = 1
            };
            if self.menu_idx == 0 {
                self.menu_idx = 2
            };

            match self.menu_idx {
                1 => {
                    self.buttons.get_mut("play").unwrap().state = EButtonState::Hover;
                    self.buttons.get_mut("exit").unwrap().state = EButtonState::Normal;
                }
                2 => {
                    self.buttons.get_mut("play").unwrap().state = EButtonState::Normal;
                    self.buttons.get_mut("exit").unwrap().state = EButtonState::Hover;
                }
                _ => unreachable!(),
            }

            // Music Control
            if rl::IsKeyPressed(rl::KeyboardKey_KEY_LEFT_BRACKET as i32) {
                *MUSIC_IDX.lock().unwrap() -= 1;

                next_music_idx(assets);
            }

            if rl::IsKeyPressed(rl::KeyboardKey_KEY_RIGHT_BRACKET as i32) {
                *MUSIC_IDX.lock().unwrap() += 1;

                next_music_idx(assets);
            }
        } //end of ready
    }

    unsafe fn draw(&mut self, assets: &mut AssetManager) {
        if self.ready {
            for button in self.buttons.values_mut() {
                button.draw(assets)
            }
        }
    }
}
