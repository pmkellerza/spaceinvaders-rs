use crate::prelude::*;

impl GameSyncWithAsset for Alien {
    unsafe fn update(&mut self, _assets: &mut AssetManager) {
        //random scaling
        self.transform.scale.x = thread_rng().gen_range(1.0..=1.1);

        self.transform.position += self.velocity;

        if self.transform.position.x >= SAFE_WIDTHF32 || self.transform.position.x <= 0.0 {
            self.velocity *= -1.0;
        }
    }

    unsafe fn draw(&mut self, assets: &mut AssetManager) {
        let texture = assets.get_texture(self.texture_id);
        rl::DrawTextureEx(
            *texture,
            self.transform.position.into_vec2(),
            self.transform.rotation.x,
            self.transform.scale.x,
            self.color.into(),
        );
    }
}
