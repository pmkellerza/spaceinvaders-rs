use crate::prelude::*;

pub struct Meteor {
    position: glm::Vec2,
    velocity: glm::Vec2,
    size: f32,

    //Coloring
    color: rl::Color,
    shimmer_color: rl::Color,
    change_color: rl::Color,
}

impl Meteor {
    pub unsafe fn new() -> Meteor {
        let mut rnd = thread_rng();

        let position = glm::Vec2::new(
            rnd.gen_range(SAFE_PADDINGF32..=SAFE_WIDTHF32),
            rnd.gen_range(SAFE_PADDINGF32..=SAFE_HEIGHTF32),
        );

        let velocity = glm::Vec2::new(0.0, rnd.gen_range(25.0..=55.0));

        let size = rnd.gen_range(2.0..=4.00);

        let color = get_color();

        Self {
            position,
            velocity,
            size,
            color,
            shimmer_color: GameColor::MINTCREAM.into(),
            change_color: color,
        }
    }

    pub unsafe fn shimmer(&mut self, num: u8) {
        match num {
            1 => self.change_color = self.color,
            2 => self.change_color = self.shimmer_color,
            3 => self.change_color = self.color,
            _ => unreachable!(),
        }
    }
}

unsafe fn get_color() -> rl::Color {
    let mut rnd = thread_rng();

    let num = rnd.gen_range(1..=3);

    return match num {
        1 => GameColor::ORANGEPEEL.into(),
        2 => GameColor::CHILLRED.into(),
        3 => GameColor::DARKMAGENTA.into(),
        _ => unreachable!(),
    };
}

pub struct MeteoriteShower {
    meteors: Vec<Meteor>,
}

impl MeteoriteShower {
    pub unsafe fn new(num: usize) -> Self {
        let mut meteors = Vec::with_capacity(num);

        for _ in 0..num {
            meteors.push(Meteor::new());
        }

        Self { meteors }
    }

    pub unsafe fn update(&mut self) {
        let mut rnd = thread_rng();

        for meteor in self.meteors.iter_mut() {
            meteor.position += meteor.velocity * rl::GetFrameTime();
            meteor.shimmer(thread_rng().gen_range(1..=3));

            if meteor.position.y >= SAFE_HEIGHTF32 {
                meteor.position = glm::Vec2::new(
                    rnd.gen_range(SAFE_PADDINGF32..SAFE_WIDTHF32),
                    SAFE_PADDINGF32,
                );
                meteor.velocity = glm::Vec2::new(0.0, rnd.gen_range(100.0..=200.0));
                meteor.color = get_color();
            }
        }
    }

    pub unsafe fn draw(&mut self) {
        for meteor in &self.meteors {
            if meteor.position.y > SAFE_PADDINGF32 {
                rl::DrawCircleV(
                    meteor.position.into_vec2(),
                    meteor.size,
                    meteor.change_color,
                );
            }
        }
    }
}
