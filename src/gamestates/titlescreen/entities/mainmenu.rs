use crate::gamestates::utils::gamestates::get_game_state;
use crate::prelude::*;

pub struct MainMenu {
    //buttons
    pub buttons: Buttons,
    pub ready: bool,

    //timer
    pub timer: f32,
    pub max_timer: f32,

    //Menu
    pub menu_idx: u8,
}

impl MainMenu {
    pub unsafe fn new() -> Self {
        //button
        let padding = 20.0;
        let width = 230.0;
        let height = 60.0;
        let pos_x = SCREEN_WIDTHF32 / 2.0 - width / 2.0;

        let mut play_btn = UIButton::new(width, height, "Play Game", GameColor::NIGHTBLACK, 30.0)
            .with_position(pos_x, 630.0);
        play_btn.function = btn_play;

        let mut exit_btn = UIButton::new(width, height, "Exit", GameColor::NIGHTBLACK, 30.0)
            .with_position(pos_x, 630.0 + height + padding);
        exit_btn.function = btn_exit;

        let mut buttons = Buttons::new();
        buttons.insert("play", play_btn);
        buttons.insert("exit", exit_btn);

        Self {
            buttons,
            ready: false,
            timer: 0.0,
            max_timer: 1.4,
            menu_idx: 1,
        }
    }
}

unsafe fn btn_play(_assets: &mut AssetManager) {
    *GAME_STATE.lock().unwrap() = get_game_state(2);
}

unsafe fn btn_exit(_assets: &mut AssetManager) {
    *CAN_QUIT.write().unwrap() = true;
}
