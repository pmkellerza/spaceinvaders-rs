use crate::prelude::*;

pub struct GameTitle {
    space: glm::Vec2,
    invader: glm::Vec2,
    space_target: glm::Vec2,
    invader_target: glm::Vec2,

    //lerping values
    lerp_space: glm::Vec2,
    lerp_invader: glm::Vec2,
}

impl GameTitle {
    pub unsafe fn new() -> Self {
        let space = glm::Vec2::new(-SCREEN_WIDTHF32, 50.0);
        let invader = glm::Vec2::new(-SCREEN_WIDTHF32, 120.0);

        let space_target = glm::Vec2::new(SCREEN_WIDTHF32 / 2.0 - 250.0, 50.0);
        let invader_target = glm::Vec2::new(SCREEN_WIDTHF32 / 2.0 - 200.0, 120.0);
        Self {
            space,
            invader,
            space_target,
            invader_target,
            lerp_space: glm::Vec2::zeros(),
            lerp_invader: glm::Vec2::zeros(),
        }
    }

    pub unsafe fn update(&mut self) {
        //lerp space title
        if self.lerp_space <= glm::Vec2::new(1.0, 1.0) {
            self.space = glm::lerp_vec(&self.space, &self.space_target, &self.lerp_space);
            //println!("space: {}", self.space);
        }

        //lerp invader title
        if self.lerp_invader <= glm::Vec2::new(1.0, 1.0) {
            self.invader = glm::lerp_vec(&self.invader, &self.invader_target, &self.lerp_invader);
            //println!("invader: {}", self.invader)
        }

        self.lerp_space += glm::Vec2::new(0.1 * rl::GetFrameTime(), 0.1 * rl::GetFrameTime());
        self.lerp_invader += glm::Vec2::new(0.055 * rl::GetFrameTime(), 0.055 * rl::GetFrameTime())
    }

    pub unsafe fn draw(&mut self, assets: &mut AssetManager) {
        if self.space.x > SAFE_PADDINGF32 {
            let space_font = assets.get_font("acrylic");
            rl::DrawTextEx(
                *space_font,
                rl_str!("SPACE"),
                self.space.into_vec2(),
                100.0,
                0.0,
                GameColor::ORANGEPEEL.into(),
            );
        }

        if self.invader.x > SAFE_PADDINGF32 {
            let invader_font = assets.get_font("acrylic");
            rl::DrawTextEx(
                *invader_font,
                rl_str!("Invaders"),
                self.invader.into_vec2(),
                80.0,
                0.0,
                GameColor::CHILLRED.into(),
            );
        }
    }
}
