use crate::prelude::*;

pub fn get_game_state(idx: u8) -> EGameState {
    FromPrimitive::from_u8(idx).unwrap()
}
