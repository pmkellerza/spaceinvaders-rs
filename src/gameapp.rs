use crate::managers::assetmanager::AssetManager;
use crate::prelude::*;

pub struct GameApp {
    //Managers
    pub assetmanager: AssetManager,
    pub statemanager: StateManager,
}

impl GameApp {
    pub unsafe fn new(fps: i32) -> Self {
        rl::InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, rl_str!(GAME_TITLE));
        if cfg!(debug_assertions) {
            rl::SetWindowMonitor(1);
        } else {
            rl::SetWindowMonitor(0);
        }

        rl::SetTargetFPS(fps);

        rl::InitAudioDevice();

        //disable the escape as quit
        rl::SetExitKey(rl::KeyboardKey_KEY_NULL as i32);

        let mut assetmanager = AssetManager::new();
        assetmanager.load_assets();

        let statemanager = StateManager::new(&mut assetmanager);
        Self {
            assetmanager,
            statemanager,
        }
    }

    pub unsafe fn update(&mut self) {
        //update call from state manager
        self.statemanager.update(&mut self.assetmanager);
    }

    pub unsafe fn draw(&mut self) {
        rl::BeginDrawing();

        //Draw call from statemanager
        self.statemanager.draw(&mut self.assetmanager);

        rl::EndDrawing();
    }
}

impl Drop for GameApp {
    fn drop(&mut self) {
        unsafe {
            //unload raylib data
            rl::CloseAudioDevice();

            rl::CloseWindow();
        }
    }
}
