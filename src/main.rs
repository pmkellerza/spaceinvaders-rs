#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
#[macro_use]
extern crate lazy_static;
extern crate num_derive;

use crate::gameapp::GameApp;
use crate::prelude::*;

mod components;
mod constants;
mod gameapp;
mod managers;
pub mod prelude;

mod gamestates;
mod traits;
mod utils;

fn main() {
    unsafe {
        let mut gameapp = GameApp::new(60);

        //game app loop
        while !*CAN_QUIT.read().unwrap() {
            gameapp.update();

            gameapp.draw();

            *CAN_QUIT.write().unwrap() = rl::WindowShouldClose() || *CAN_QUIT.read().unwrap();
        }
    }
}
