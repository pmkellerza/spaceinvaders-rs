use crate::prelude::*;

#[derive(Copy, Clone, Debug)]
pub struct GameColor {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
}

impl GameColor {
    pub const fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }
}

impl From<GameColor> for rl::Color {
    fn from(value: GameColor) -> Self {
        Self {
            r: value.r,
            g: value.g,
            b: value.b,
            a: value.a,
        }
    }
}

impl GameColor {
    pub const ORANGEPEEL: GameColor = GameColor::new(255, 152, 0, 255);
    pub const NIGHTBLACK: GameColor = GameColor::new(20, 20, 25, 255);
    pub const CORNELLRED: GameColor = GameColor::new(191, 33, 30, 255);
    pub const ICEBLUE: GameColor = GameColor::new(14, 107, 168, 255);
    pub const DARKMAGENTA: GameColor = GameColor::new(124, 35, 140, 255);
    pub const KELLYGREEN: GameColor = GameColor::new(43, 192, 22, 255);
    pub const DARKPURPLE: GameColor = GameColor::new(36, 16, 35, 255);
    pub const CHILLRED: GameColor = GameColor::new(234, 43, 31, 255);
    pub const PAYNEGRAY: GameColor = GameColor::new(93, 115, 126, 255);
    pub const MINTCREAM: GameColor = GameColor::new(240, 247, 238, 255);
    pub const RAYWHITE: GameColor = GameColor::new(245, 245, 245, 255);
    pub const WHITE: GameColor = GameColor::new(255, 255, 255, 255);
}
