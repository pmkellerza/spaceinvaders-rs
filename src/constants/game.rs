#![allow(dead_code)]
use crate::prelude::EGameState;
use std::sync::{Mutex, RwLock};

lazy_static! {
    pub static ref CAN_QUIT: RwLock<bool> = RwLock::new(false);
    pub static ref CURRENT_MUSIC_TRACK: Mutex<&'static str> = Mutex::new("track_01");
    pub static ref MUSIC_IDX: Mutex<u8> = Mutex::new(1);
    pub static ref GAME_STATE: Mutex<EGameState> = Mutex::new(EGameState::TitleScreen);
    pub static ref GAME_ENDED: Mutex<bool> = Mutex::new(false);
    pub static ref GAME_CREATED: Mutex<bool> = Mutex::new(false);
}

//Screen Resolution
pub const SCREEN_WIDTH: i32 = 800;
pub const SCREEN_WIDTHF32: f32 = SCREEN_WIDTH as f32;
pub const SCREEN_HEIGHT: i32 = 900;
pub const SCREEN_HEIGHTF32: f32 = SCREEN_HEIGHT as f32;

//Safe Rendering Zone
pub const SAFE_PADDING: i32 = 20;
pub const SAFE_PADDINGF32: f32 = SAFE_PADDING as f32;
pub const SAFE_WIDTH: i32 = 760;
pub const SAFE_WIDTHF32: f32 = SAFE_WIDTH as f32;
pub const SAFE_HEIGHT: i32 = 860;
pub const SAFE_HEIGHTF32: f32 = SAFE_HEIGHT as f32;

//Game Constants
pub const GAME_TITLE: &str = "--- Space Invaders ---";

//JukeBox
pub const MAX_MUSIC_TRACKS: u8 = 3;
