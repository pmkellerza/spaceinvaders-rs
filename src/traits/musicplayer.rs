pub trait MusicPlayer {
    unsafe fn play_music(&mut self, track: impl Into<&'static str>);
    unsafe fn update_music(&mut self, track: impl Into<&'static str>);
    unsafe fn stop_music(&mut self, track: impl Into<&'static str>);
    unsafe fn pause_music(&mut self, track: impl Into<&'static str>);
    unsafe fn resume_music(&mut self, track: impl Into<&'static str>);
    unsafe fn is_music_done(&mut self, track: impl Into<&'static str>) -> bool;
    unsafe fn get_timeplayed(&mut self, track: impl Into<&'static str>) -> f32;
    unsafe fn get_music_length(&mut self, track: impl Into<&'static str>) -> f32;
    unsafe fn is_playing_music(&mut self, track: impl Into<&'static str>) -> bool;
}
