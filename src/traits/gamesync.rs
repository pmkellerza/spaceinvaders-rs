use crate::prelude::*;

pub trait GameSync {
    unsafe fn update(&mut self);
    unsafe fn draw(&mut self);
}

pub trait GameSyncWithAsset {
    unsafe fn update(&mut self, assets: &mut AssetManager);
    unsafe fn draw(&mut self, assets: &mut AssetManager);
}

pub trait GameSyncWithAssetState {
    unsafe fn update(&mut self, assets: &mut AssetManager, state: &mut StateManager);
    unsafe fn draw(&mut self, assets: &mut AssetManager);
}
