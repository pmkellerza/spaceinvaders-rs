use crate::managers::utils::assetloader::*;

use crate::prelude::*;

pub struct AssetManager {
    //Data Storage
    pub textures: Textures,
    pub sounds: Sounds,
    pub fonts: Fonts,
    pub jukebox: JukeBox,
    pub shaders: Shaders,
}

impl AssetManager {
    pub unsafe fn new() -> Self {
        Self {
            textures: Textures::new(),
            sounds: Sounds::new(),
            fonts: Fonts::new(),
            jukebox: JukeBox::new(),
            shaders: Shaders::new(),
        }
    }

    pub unsafe fn load_assets(&mut self) {
        //Load Fonts
        load_fonts(&mut self.fonts);

        //Load Shaders
        load_shaders(&mut self.shaders);

        //Load Textures
        load_textures(&mut self.textures);

        //Load Music
        load_music(&mut self.jukebox);

        //Load Sounds
        load_sounds(&mut self.sounds);
    }

    pub unsafe fn get_texture(&mut self, id: impl Into<&'static str>) -> &mut rl::Texture2D {
        let id = id.into();
        self.textures.get_mut(id).unwrap()
    }

    pub unsafe fn get_mut_font(&mut self, id: impl Into<&'static str>) -> &mut rl::Font {
        let id = id.into();
        self.fonts.get_mut(id).unwrap()
    }

    pub unsafe fn get_font(&mut self, id: impl Into<&'static str>) -> &rl::Font {
        let id = id.into();
        self.fonts.get(id).unwrap()
    }

    pub unsafe fn get_sound(&mut self, id: impl Into<&'static str>) -> &mut rl::Sound {
        let id = id.into();
        self.sounds.get_mut(id).unwrap()
    }

    pub unsafe fn get_musictrack(&mut self, id: impl Into<&'static str>) -> &MusicTrack {
        let id = id.into();
        self.jukebox.get(id).unwrap()
    }

    pub unsafe fn get_raylib_music(&mut self, id: impl Into<&'static str>) -> &mut rl::Music {
        let id = id.into();
        let musictrack = self.jukebox.get_mut(&id).unwrap();

        musictrack.get_track()
    }

    pub unsafe fn get_shader(&mut self, id: impl Into<&'static str>) -> &mut rl::Shader {
        let id = id.into();
        self.shaders.get_mut(id).unwrap()
    }
}

impl Drop for AssetManager {
    fn drop(&mut self) {
        unsafe {
            //textures drop
            for (_, texture) in self.textures.drain() {
                rl::UnloadTexture(texture);
            }

            //Sound
            for (_, sound) in self.sounds.drain() {
                rl::UnloadSound(sound);
            }

            //Fonts
            for (_, font) in self.fonts.drain() {
                rl::UnloadFont(font);
            }

            //Music
            for (_, musictrack) in self.jukebox.drain() {
                rl::UnloadMusicStream(musictrack.music);
            }

            //shaders
            for (_, shader) in self.shaders.drain() {
                rl::UnloadShader(shader);
            }
        }
    }
}
