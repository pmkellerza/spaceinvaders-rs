use crate::prelude::*;

pub unsafe fn load_fonts(fonts: &mut Fonts) {
    let font = rl::LoadFontEx(rl_str!("./assets/fonts/acrylic.otf"), 64, 0 as *mut i32, 0);
    fonts.insert("acrylic", font);

    let font = rl::LoadFontEx(rl_str!("./assets/fonts/scorn.otf"), 64, 0 as *mut i32, 0);
    fonts.insert("scorn", font);

    let font = rl::LoadFontEx(rl_str!("./assets/fonts/sonoma.otf"), 64, 0 as *mut i32, 0);
    fonts.insert("sonoma", font);
}

pub unsafe fn load_shaders(shaders: &mut Shaders) {
    let shader = rl::LoadShader(rl_str!(0), rl_str!("./assets/shaders/bloom.glsl"));
    shaders.insert("bloom", shader);
}

pub unsafe fn load_textures(textures: &mut Textures) {
    //mysteryship
    let texture = rl::LoadTexture(rl_str!("./assets/textures/mystery.png"));
    textures.insert("mysteryship", texture);

    //alienships
    let texture = rl::LoadTexture(rl_str!("./assets/textures/alien_1.png"));
    textures.insert("alien_1", texture);
    let texture = rl::LoadTexture(rl_str!("./assets/textures/alien_2.png"));
    textures.insert("alien_2", texture);
    let texture = rl::LoadTexture(rl_str!("./assets/textures/alien_3.png"));
    textures.insert("alien_3", texture);

    //ui
    let texture = rl::LoadTexture(rl_str!("./assets/textures/ui_border.png"));
    textures.insert("ui_border", texture);

    //UI buttons
    let texture = rl::LoadTexture(rl_str!("./assets/textures/ui_button_01.png"));
    textures.insert("ui_button01", texture);
    let texture = rl::LoadTexture(rl_str!("./assets/textures/ui_button_01_hover.png"));
    textures.insert("ui_button01hover", texture);
    let texture = rl::LoadTexture(rl_str!("./assets/textures/ui_button_01_select.png"));
    textures.insert("ui_button01select", texture);

    //Pause Menu textures
    let texture = rl::LoadTexture(rl_str!("./assets/textures/pausemenu_backgnd.png"));
    textures.insert("pmenu_backgnd", texture);
}

pub unsafe fn load_music(jukebox: &mut JukeBox) {
    let musictrack = MusicTrack::new("Julia", "Grand Project", "track_01.mp3");
    jukebox.insert("track_01", musictrack);

    let musictrack = MusicTrack::new("Starchill", "LFC Records", "track_02.mp3");
    jukebox.insert("track_02", musictrack);

    let musictrack = MusicTrack::new("Back To The 80s", "Rovador", "track_03.mp3");
    jukebox.insert("track_03", musictrack);
}

pub unsafe fn load_sounds(sounds: &mut Sounds) {
    let sound = rl::LoadSound(rl_str!("./assets/sounds/UI_MenuSelections.wav"));
    rl::SetSoundVolume(sound, 0.35);
    sounds.insert("menu_select", sound);
}
