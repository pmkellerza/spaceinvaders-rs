use crate::prelude::*;

impl MusicPlayer for JukeBox {
    unsafe fn play_music(&mut self, track: impl Into<&'static str>) {
        rl::PlayMusicStream(get_rlmusic(self, track.into()));
    }

    unsafe fn update_music(&mut self, track: impl Into<&'static str>) {
        rl::UpdateMusicStream(get_rlmusic(self, track.into()));
    }

    unsafe fn stop_music(&mut self, track: impl Into<&'static str>) {
        rl::StopMusicStream(get_rlmusic(self, track.into()));
    }
    unsafe fn pause_music(&mut self, track: impl Into<&'static str>) {
        rl::PauseMusicStream(get_rlmusic(self, track.into()));
    }
    unsafe fn resume_music(&mut self, track: impl Into<&'static str>) {
        rl::ResumeMusicStream(get_rlmusic(self, track.into()));
    }
    unsafe fn is_music_done(&mut self, track: impl Into<&'static str>) -> bool {
        let music = get_rlmusic(self, track.into());
        let timeplayed = rl::GetMusicTimePlayed(music) / rl::GetMusicTimeLength(music);

        if timeplayed >= 0.998 {
            true
        } else {
            false
        }
    }

    unsafe fn get_timeplayed(&mut self, track: impl Into<&'static str>) -> f32 {
        let music = get_rlmusic(self, track.into());
        let played = rl::GetMusicTimePlayed(music);
        let len = rl::GetMusicTimeLength(music);

        (played / len) * 100.0
    }

    unsafe fn get_music_length(&mut self, track: impl Into<&'static str>) -> f32 {
        rl::GetMusicTimeLength(get_rlmusic(self, track.into()))
    }

    unsafe fn is_playing_music(&mut self, track: impl Into<&'static str>) -> bool {
        let music = get_rlmusic(self, track.into());
        rl::IsMusicStreamPlaying(music)
    }
}

fn get_rlmusic(jukebox: &mut JukeBox, track: &'static str) -> rl::Music {
    jukebox.get_mut(track).unwrap().music
}

pub unsafe fn next_music_idx(assets: &mut AssetManager) {
    assets
        .jukebox
        .stop_music(*CURRENT_MUSIC_TRACK.lock().unwrap());

    let mut idx = *MUSIC_IDX.lock().unwrap();

    if idx == MAX_MUSIC_TRACKS + 1 {
        idx = 1;
    }

    if idx == 0 {
        idx = MAX_MUSIC_TRACKS;
    }
    *MUSIC_IDX.lock().unwrap() = idx;

    //track selection
    match *MUSIC_IDX.lock().unwrap() {
        1 => *CURRENT_MUSIC_TRACK.lock().unwrap() = "track_01",
        2 => *CURRENT_MUSIC_TRACK.lock().unwrap() = "track_02",
        3 => *CURRENT_MUSIC_TRACK.lock().unwrap() = "track_03",
        _ => {}
    }

    assets
        .jukebox
        .play_music(*CURRENT_MUSIC_TRACK.lock().unwrap());
}

pub unsafe fn update_jukebox(assets: &mut AssetManager) {
    assets
        .jukebox
        .update_music(*CURRENT_MUSIC_TRACK.lock().unwrap());

    if assets
        .jukebox
        .is_music_done(*CURRENT_MUSIC_TRACK.lock().unwrap())
    {
        *MUSIC_IDX.lock().unwrap() += 1;

        next_music_idx(assets);
    }
}
