use crate::prelude::*;

pub struct MusicTrack {
    pub name: String,
    pub artist: String,
    pub music: rl::Music,
}

impl MusicTrack {
    pub unsafe fn new(
        name: impl Into<String>,
        artist: impl Into<String>,
        file: impl Into<String>,
    ) -> Self {
        let name = name.into();
        let artist = artist.into();
        let file = file.into();

        let file = format!("assets/music/{}", file);
        let music = rl::LoadMusicStream(rl_str!(file));
        rl::SetMusicVolume(music, 0.25);
        Self {
            name,
            artist,
            music,
        }
    }

    pub unsafe fn default(file: impl Into<String>) -> Self {
        let name = "".into();
        let artist = "".into();
        let file = file.into();

        let file = format!("assets/music/{}", file);
        let music = rl::LoadMusicStream(rl_str!(file));
        rl::SetMusicVolume(music, 0.25);
        Self {
            name,
            artist,
            music,
        }
    }

    pub unsafe fn get_track_name(&mut self) -> &mut String {
        &mut self.name
    }
    pub unsafe fn get_artist(&mut self) -> &mut String {
        &mut self.artist
    }
    pub unsafe fn get_track(&mut self) -> &mut rl::Music {
        &mut self.music
    }
}
