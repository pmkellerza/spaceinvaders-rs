use crate::prelude::*;

#[repr(u8)]
#[derive(FromPrimitive, ToPrimitive, Debug, Copy, Clone)]
pub enum EGameState {
    TitleScreen = 1,
    GameMode,
    PauseMenu,
}

impl From<u8> for EGameState {
    fn from(value: u8) -> Self {
        match value {
            1 => EGameState::TitleScreen,
            2 => EGameState::GameMode,
            3 => EGameState::PauseMenu,
            _ => unreachable!(),
        }
    }
}

pub struct StateManager {
    //Gamestates
    pub titlescreen: TitleScreen,
    pub spaceinvaders: Option<SpaceInvaders>, //EGamestate::GameMode
    pub pausemenu: PauseMenu,

    pub current_state: EGameState,
}

impl StateManager {
    pub unsafe fn new(assets: &mut AssetManager) -> Self {
        Self {
            //States
            titlescreen: TitleScreen::new(assets),
            spaceinvaders: None,
            pausemenu: PauseMenu::new(assets),
            //Other
            current_state: EGameState::TitleScreen,
        }
    }

    pub unsafe fn update(&mut self, assets: &mut AssetManager) {
        self.current_state = *GAME_STATE.lock().unwrap();

        //check Game Ended Status
        if *GAME_ENDED.lock().unwrap() {
            self.spaceinvaders = None;
            *GAME_ENDED.lock().unwrap() = false;
        }

        match self.current_state {
            EGameState::TitleScreen => {
                self.titlescreen.update(assets);
            }
            EGameState::GameMode => {
                //create Game if doesnt exist
                if let None = &mut self.spaceinvaders {
                    self.spaceinvaders = Some(SpaceInvaders::new(assets));

                    *GAME_CREATED.lock().unwrap() = true;
                    println!("SpaceInvaders Created");
                }

                if let Some(s) = &mut self.spaceinvaders {
                    s.update(assets)
                }
            }
            EGameState::PauseMenu => {
                self.pausemenu.update(assets);
            }
        }
    }
    pub unsafe fn draw(&mut self, assets: &mut AssetManager) {
        match self.current_state {
            EGameState::TitleScreen => {
                self.titlescreen.draw(assets);
            }
            EGameState::GameMode => {
                if let Some(s) = &mut self.spaceinvaders {
                    s.draw(assets)
                }
            }
            EGameState::PauseMenu => {
                self.pausemenu.draw(assets);
            }
        }
    }
}
