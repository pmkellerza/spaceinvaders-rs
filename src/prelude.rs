#![allow(unused_imports)]
#![cfg_attr(rustfmt, rustfmt_skip)]

//core
pub use num_derive::FromPrimitive;
pub use num_derive::ToPrimitive;
pub use num_traits::FromPrimitive;
pub use num_traits::ToPrimitive;

// Raylib 5 FFI Imports
pub use raylib5_rs::ray_ffi as rl;
pub use raylib5_rs::rl_str;

//nalebra-glm
pub extern crate nalgebra_glm as glm;
pub use rand::prelude::*;

//Game Internals
pub use crate::utils::{types::*, conversion::*};
pub use crate::traits::{gamesync::*, musicplayer::*};
pub use crate::constants::{colours::*, game::*};
pub use crate::components::{
    transform::*,
    button::*,
};

//Managers and their entities
pub use crate::managers::{
    statemanager::*, 
    assetmanager::*, 
    entities:: {musictrack::*,},
    utils::{jukebox::*},
};

//Gamestates and their entities
pub use crate::gamestates::{
    titlescreen::{TitleScreen,entities::meteorites::MeteoriteShower},
    spaceinvaders::{SpaceInvaders,entities::*},
    pausemenu::{PauseMenu}
};
pub use crate::gamestates::spaceinvaders::{
    entities::{mysteryship::*, starfield::*, alien::*,},
};